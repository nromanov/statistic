# Deposit service

Simple service for displaying Deposit service response

User data - UserViewController

Deposit dat - DepositViewController

## Getting Started

Please, run DepositApplication.class to up and run Deposit service
Then run StatisticApplication.class to up and run Statistic service 
You can change port in application.properties.
```
server.port:8081
```

### Api

Swagger is available after service is up and running by link:

```
http://localhost:8081/swagger-ui.html#/
```

### UI

After service up and run. You can see response on Web page:

```
http://localhost:8081/api/v1/showUsers
http://localhost:8081/api/v1/showDeposits

```
### Mock server

Mock server available via docker-compose.
To setup it, please run TestMockServer after Mock up and running.

### Docker-compose

After success build (see gitlab ci) new image will pushed
in registry. Compose files (with service or mock) are available
in project.

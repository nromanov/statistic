FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} statistic.jar
ENTRYPOINT ["java","-jar","/statistic.jar"]
package com.example.statistic;

import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class TestMockServer {

    private ClientAndServer mockServer;

    @BeforeClass
    public void startServer() {
        mockServer = startClientAndServer(1080);
    }

    @AfterClass
    public void stopServer() {
        mockServer.stop();
    }

    @Test
    private void getAllUsers() throws IOException {
        mock(
                request()
                        .withMethod("GET")
                        .withPath("/api/v1/user/"),
                response()
                        .withStatusCode(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(com.google.common.io.Files.toString(
                                new File("src/test/resources/users.json"),
                                Charset.defaultCharset()))
        );
    }

    @Test
    private void getAnyUser() throws IOException {
        mock(
                request()
                        .withMethod("GET")
                        .withPath("/api/v1/user/[0-9]{1,2}"),
                response()
                        .withStatusCode(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(com.google.common.io.Files.toString(
                                new File("src/test/resources/user1.json"),
                                Charset.defaultCharset()))
        );
    }

    private void mock(HttpRequest request, HttpResponse response) {
        mockServer.clear(request);
        mockServer.when(request).respond(response);
    }
}
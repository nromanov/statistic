package com.example.statistic;

import com.dockerdemo.statistic.StatisticApplication;
import com.dockerdemo.statistic.annotation.InjectRandomInt;
import com.dockerdemo.statistic.domain.Deposit;
import com.dockerdemo.statistic.domain.User;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.stream.Collectors;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;
import static io.restassured.RestAssured.given;

@SpringBootTest(classes = {StatisticApplication.class},
		webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class StatisticApplicationTests extends AbstractTestNGSpringContextTests {

	@InjectRandomInt(min = 1, max = 50)
	private int userNumber;

	@Value("${users.urlUi}")
	private String usersUrlUi;

	@Value("${user.url}")
	private String userUrlServices;

	@Value("${user.count}")
	private int userCount;

	private User expectedUser;

	RequestSpecification rSpec() {
		return new RequestSpecBuilder()
				.setContentType(ContentType.JSON)
				.setAccept(ContentType.JSON)
				.build();
	}

	@BeforeClass
	public void getUser() {
		expectedUser = given().spec(rSpec())
				.pathParam("id", userNumber)
				.get(userUrlServices + "{id}")
				.then().statusCode(HttpStatus.OK.value())
				.extract().body().as(User.class);
	}

	@Test
	public void showUsers() {
		open(usersUrlUi);
		$$("table tbody tr").shouldHave(size(userCount));
		$(byName("userId")).setValue(String.valueOf(userNumber));
		$(byAttribute("type", "submit")).click();

		$("table tbody tr", 0)
				.shouldHave(text(String.valueOf(expectedUser.getId())),
						text(expectedUser.getFirstName()),
						text(expectedUser.getLastName()),
						text(expectedUser.getDeposits().stream().map(Deposit::toString)
								.collect(Collectors.joining(" ,")))
				);
	}
}
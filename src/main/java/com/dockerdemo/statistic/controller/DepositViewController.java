package com.dockerdemo.statistic.controller;

import com.dockerdemo.statistic.domain.Deposit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@RestController
@RequestMapping("/api/v1")
@Api(value = "Deposit Management")
public interface DepositViewController {

    @GetMapping(value = {"/showDeposits", "/showDeposits/{id}"})
    @ApiOperation(value = "Get all deposits or all deposit by user", response = Deposit.class,
            responseContainer = "List")
    ModelAndView showDeposits(
            @ApiParam(value = "Deposit id") @Nullable @PathVariable Integer id,
            @ApiParam(value = "Deposit id") @Nullable @RequestParam Integer depositId,
            @ApiParam(value = "User id") @Nullable @RequestParam Integer userId);
}
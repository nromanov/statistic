package com.dockerdemo.statistic.controller;

import com.dockerdemo.statistic.domain.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/v1")
@Api(value = "User Management")
public interface UserViewController {

    @GetMapping(value = {"/showUsers", "/showUsers/{id}"})
    @ApiOperation(value = "Get all users.json or all users.json by id", response = User.class,
            responseContainer = "List")
    ModelAndView showUsers(
            @ApiParam(value = "User id") @Valid @Nullable @PathVariable Integer id,
            @ApiParam(value = "User id") @Valid @Nullable @RequestParam Integer userId);

    @GetMapping(value = "/user/{id}")
    @ApiOperation(value = "Get one user", response = User.class)
    ResponseEntity<User> getUser (
            @ApiParam(value = "User id") @Valid @NotNull @PathVariable Integer id
    );
}
package com.dockerdemo.statistic.contollerImpl;

import com.dockerdemo.statistic.controller.UserViewController;
import com.dockerdemo.statistic.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserViewControllerImpl implements UserViewController {

    RestTemplate restTemplate = new RestTemplate();

    @Value("${user.url}")
    private String userUrl;

    public ModelAndView showUsers(Integer id, Integer userId) {
        RestTemplate restTemplate = new RestTemplate();
        if (id != null) {
            return getView(restTemplate, id);
        } else if (userId != null) {
            return getView(restTemplate, userId);
        }
        ResponseEntity<User[]> response = restTemplate.getForEntity(userUrl, User[].class);
        Map<String, Object> params = new HashMap<>();
        params.put("users", Arrays.asList(response.getBody()));
        return new ModelAndView("showUsers", params);
    }

    @Override
    public ResponseEntity<User> getUser(Integer id) {
        return restTemplate.getForEntity(userUrl + id, User.class);
    }

    private ModelAndView getView(RestTemplate template, Integer id) {
        ResponseEntity<User> response = template.getForEntity(userUrl + id, User.class);
        Map<String, Object> params = new HashMap<>();
        params.put("users", Arrays.asList(response.getBody()));
        return new ModelAndView("showUsers", params);
    }
}
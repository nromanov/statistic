package com.dockerdemo.statistic.contollerImpl;

import com.dockerdemo.statistic.controller.DepositViewController;
import com.dockerdemo.statistic.domain.Deposit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
public class DepositViewControllerImpl implements DepositViewController {

    @Value("${deposit.url}")
    private String depositUrl;

    public ModelAndView showDeposits(Integer id, Integer depositId, Integer userId) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Deposit[]> response;
        if (id != null) {
            return getView(restTemplate, depositUrl + id);
        } else if (depositId != null) {
            return getView(restTemplate, depositUrl + depositId);
        } else if(userId != null) {
            response = restTemplate.getForEntity(depositUrl + "all/" + userId, Deposit[].class);
        } else {
            response = restTemplate.getForEntity(depositUrl, Deposit[].class);
        }
        Map<String, Object> params = new HashMap<>();
        params.put("deposits", Arrays.asList(response.getBody()));
        return new ModelAndView("showDeposits", params);
    }

    private ModelAndView getView(RestTemplate template, String url) {
        ResponseEntity<Deposit> response = template.getForEntity(url, Deposit.class);
        Map<String, Object> params = new HashMap<>();
        params.put("deposits", Arrays.asList(response.getBody()));
        return new ModelAndView("showDeposits", params);
    }
}
package com.dockerdemo.statistic.config;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;


@ControllerAdvice
public class ExceptionControllerAdvice {
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(Throwable.class)
    public ModelAndView genericExceptionHandler(Exception ex) {
        String errorType;
        if(ex instanceof HttpClientErrorException.NotFound) {
            errorType = "NOT_FOUND";
        } else if(ex instanceof HttpClientErrorException.BadRequest ||
            ex instanceof TypeMismatchException) {
            errorType = "BAD_REQUEST";
        } else {
            errorType = "INTERNAL_SERVER_ERROR";
        }
        String errorMessage = ex.getMessage();
        ModelAndView mv = new ModelAndView("errors/error");
        Map<String, Object> model = mv.getModelMap();
        model.put("errorMessage", errorMessage);
        model.put("errorType", errorType);
        return mv;
    }
}
package com.dockerdemo.statistic.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
public class Deposit {

    private Integer id;

    private User user;

    private Integer curDepositBalance;

    private Date dateTime;

    private Set<Transaction> transactions = new HashSet<>();

    @JsonIgnore
    private Integer transactionsAmount;

    public Deposit() {
    }

    public Deposit(User user, Integer balance, Date dateTime) {
        this.user = user;
        this.curDepositBalance = balance;
        this.dateTime = dateTime;
    }

    public void setUser(User user) {
        user.getDeposits().add(this);
        this.user = user;
    }

    public Integer getTransactionsAmount() {
        return transactions.stream()
                .mapToInt(Transaction::getTransactionAmount).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return id.equals(deposit.id) &&
                user.equals(deposit.user) &&
                curDepositBalance.equals(deposit.curDepositBalance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, curDepositBalance);
    }
}

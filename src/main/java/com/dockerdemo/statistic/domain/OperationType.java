package com.dockerdemo.statistic.domain;

public enum OperationType {
    RECHARGE,
    WRITE_OFF
}

package com.dockerdemo.statistic.domain;

import lombok.Data;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Data
public class Transaction {

    private UUID id;

    private Deposit deposit;

    private Integer transactionAmount;

    private OperationType operation;

    private Date dateTime;

    private Integer depositBefore;

    public Transaction() {
    }

    public Transaction(Deposit deposit, Integer transaction_amount, Integer depositBefore, OperationType type, Date dateTime) {
        this.deposit = deposit;
        this.transactionAmount = transaction_amount;
        this.dateTime = dateTime;
        this.operation = type;
        this.depositBefore = depositBefore;
    }

    public void setDeposit(Deposit deposit) {
        deposit.getTransactions().add(this);
        this.deposit = deposit;
    }

    @Override
    public String toString() {
        return "Transaction {" +
                " transaction_id=" + id +
                ", transactionAmount=" + transactionAmount +
                ", operation=" + operation +
                ", dateTime=" + dateTime +
                ", depositBefore=" + depositBefore +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(deposit, that.deposit) &&
                Objects.equals(transactionAmount, that.transactionAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, deposit, transactionAmount);
    }
}
